<?php
include "bd/connect.php";
function get_site_map_content()
{
    $xml = "";
    header('Content-type: text/xml; charset="utf-8"');
    $home = 'https://menudosgift.es/';
    $xml .= '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
    $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    $con = connect();
    if($con) {
            $rslt = 'SELECT * FROM myposts ORDER BY fecha DESC';
            if($res = $con->query($rslt)) {
                $row = $res->fetch_row();
                $xml .= "<url><loc>".$home."</loc><lastmod>".date("Y-m-d", strtotime($row[6]))."</lastmod><changefreq>daily</changefreq></url>";
                while($row = $res->fetch_assoc())
                {
                    $xml .= "<url><loc>".$home."post-".$row['id']."/".str_replace(' ','_',cleanSpecialChar(strtolower($row['info']))).".html</loc><lastmod>".date("Y-m-d", strtotime( $row['fecha']))."</lastmod></url>";
                };
                $tags = 'SELECT * FROM mytags ORDER BY id DESC';
                if($res2 = $con->query($tags))
                {
                    while($row2 = $res2->fetch_assoc())
                    {
                        $xml .= "<url><loc>".$home."tag-".$row2['id']."/".str_replace(' ','_',cleanSpecialChar(strtolower($row2['tag'])))."</loc></url>";
                        //$xml .= "\t<url><lastmod>".$row['fecha']."</lastmod>\r\n\t</url>\r\n";
                    }
                }
                $categories = 'SELECT * FROM categories ORDER BY id DESC';
                if($res3 = $con->query($categories))
                {
                    while($row3 = $res3->fetch_assoc())
                    {
                        $xml .= "<url><loc>".$home."category-".$row3['id']."/".str_replace(' ','_',cleanSpecialChar(strtolower($row3['category'])))."</loc></url>";
                    }
                }
            }
            $xml .= '</urlset>';
    }
    return $xml;
}
function cleanSpecialChar($str)
{
    $string = trim($str);
    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );
    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );
    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );
    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );
    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );
    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("\\", "¨", "º", "-", "~",
            "#", "@", "|", "!", "\"",
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "[", "^", "`", "]",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            "."),
        '',
        $string
    );
    return $string;

}
echo get_site_map_content();

