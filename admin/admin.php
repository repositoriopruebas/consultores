<?php
include '../php/response.php';
include '../bd/update-image.php';

if(isset($_SESSION['username']))
{   
    $imageprofile = "<img class ='rounded-circle w-25 img-fluid ' src='data:image/png;base64,".queryUpdate($_SESSION['username']->iduser)."'>";
}else
{
    header('Location:../index.php');
}
?>
<html>
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Material Design for Bootstrap</title>
    <!-- MDB icon -->
    <link rel="icon" href="../img/mdb-favicon.ico" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="../css/style.css">   
    <script
                src="https://code.jquery.com/jquery-2.2.4.min.js"
                integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                crossorigin="anonymous"></script>             
    </head>
    <body>
        <div class="row d-flex justify-content-center">        
            <div class="col-lg-4 col-md-12 mb-4">
                <!--Card-->
                <div class="card testimonial-card">

                <!--Background color-->
                <div class="card-up teal lighten-2">
                </div>

                <!--Avatar-->
                <div class="avatar mx-auto white text-center">
                <?php echo $imageprofile ?>
                
                </div>
                <div class="card-body">
                    <!--Name-->
                    <h5 class="card-title mt-1 text-center">Nombre: <?php echo $_SESSION['username']->name ?></h5>
                    <h5 class="card-title mt-1 text-center">email: <?php echo $_SESSION['username']->email ?></h5>
                    <hr>
                    <!--Quotation-->
                    <p><i class="fas fa-quote-left"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos,
                    adipisci.</p>
                    <p><a href="../index.php">Volver a la home</a></p>
                </div>

                </div>
                <!--Card-->

            </div>
        <!--Grid column-->
        </div>
        
        <form class="text-center border border-light p-5"  action="imageprocess.php" method="POST" enctype="multipart/form-data">
            <label >Sube una imagen para tu avatar</label><br>
            <input required type="file" name="file">
            <input type="submit" name="send" value="enviar">
        </form>       
    </body>
</html>