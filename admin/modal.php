<head>
    <link rel="icon" href="/img/mdb-favicon.ico" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <!-- Google Fonts Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../css/mdb.min.css">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/estilos.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script>
    <script src="../js/eventuser.js"> </script>
</head>
<div  class="modal fade right" id="fullHeightModalTop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
    <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
    <div class="modal-dialog modal-full-height modal-top" role="document">
        <div class="modal-content">
            <input type="hidden" id="idoculto">
            <div class="modal-header d-flex flex-row align-items-around ">
                <h4 class="modal-title w-100 ml-4" id="myModalLabel">Error de entrada</h4>
                <ul id="profile-link-nav" style="list-style: none" class="d-flex flex-row align-items-center justify-content-around w-100 mt-3 lead navbar-nav mr-auto row">
                    <li><a class="default-link" id="profile-link-image" data-value="formulary">Sube tu imagen de perfil</a></li>
                    <li><a id="profile-link-quote" data-value="text-area">Descripción</a></li>
                    <li><a id="profile-link-post"  data-value="post">Escribe tu comentario</a></li>
                    <li><a id="profile-link-edit"  data-value="delete">Editar registros</a></li>
                </ul>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body ">
                <div class="row d-flex justify-content-around">
                    <div class="col-lg-4 col-md-12 mb-4">
                        <!--Card-->
                        <div class="card testimonial-card">
                            <!--Background color-->
                            <div class="card-up teal lighten-2">
                            </div>
                            <!--Avatar-->
                            <div class="avatar mx-auto white text-center image">
                                <img width="40%" id="profile-img" class="img-fluid rounded" src="" alt="">
                            </div>
                            <div id="info-profile" class="card-body">
                                <!--Name-->
                                <h5 id="name-profile" class="card-title mt-1 text-center"></h5>
                                <h5 id="email-profile" class="card-title mt-1 text-center"></h5>
                                <hr>
                                <!--Quotation-->
                                <p><i class="fas fa-quote-left"></i>Pruebe otra manera más adecuada para entrar a su administrador.</p>

                            </div>
                        </div>
                        <!--Card-->
                    </div>
                    <!--Grid column-->
                    <div id="components-profile" class="d-flex flex-row w-50 justify-content-center">
                        <div class="w-33 form-profile form-group">

                        </div>
                        <div id="profile-description" class="w-33 quote-profile md-form secondary-textarea active-secondary-textarea-2">
                            <form style="display: none" id="text-area">

                                <textarea class="md-textarea  form-control text-dark" onkeypress="countChars()" name="text" id="quote" cols="38" rows="6" placeholder="Añade una breve descripción de ti"></textarea>
                                <button onclick='buttonQuote()' class="btn btn-sm btn-outline-secondary w-100 waves-effect" type="button" id="boton">enviar</button>
                                <label id="tags" for=""></label>
                                <p id="countchars"></p>
                            </form>
                        </div>
                        <div style="max-width:55%" class= "grid form-group quote-profile md-form secondary-textarea active-secondary-textarea-2">
                            <form  id="post">
                                <div class="row">
                                    <div id="panelcategory" class="mb-5 col-md-3">

                                    </div>
                                    <div id="categories" class="col-md-3">

                                    </div>
                                    <div class="col-md-5">
                                        <textarea   class="md-textarea form-control text-dark" name="profile-post" id="posts" cols="38" rows="6"></textarea>
                                        <div class="btn-group">
                                            <button  type="button" id="submit-post-button" class="btn btn-sm btn-outline-secondary ">Publicar </button>
                                            <button  type="button" id="submit-createtag-button" class="btn btn-sm btn-outline-secondary">Ver tags </button>
                                        </div>
                                    </div>
                                </div>
                                <!-- <button type="button" id="post-button" class="btn btn-sm btn-outline-secondary w-100 waves-effect">Listado etiquetas clave</button>-->
                                <!--<button  type="button" id="submit-viewquery-button" class="btn btn-sm btn-outline-secondary  waves-effect">Ver entradas </button>-->
                                <p style="display:block;" class="lead labeltags mt-3 mr-4">Etiquetas a elegir</p><br>
                            </form>
                            <form style="width:50%" id="img-post" class="text-center  p-3"  action="#" method="POST" enctype="multipart/form-data">
                                <input class="btn btn-sm btn-outline-secondary waves-effect" type="file" id="file-post-image">
                                <button class="btn btn-sm btn-outline-secondary waves-effect" type="button" id="send-image-post">Enviar</button>
                            </form>
                        </div>
                        <div class="w-33 form-group">
                            <form id="delete" >
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>