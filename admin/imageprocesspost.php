<?php
/**
 * Created by PhpStorm.
 * User: usuario
 * Date: 21/11/20
 * Time: 8:53
 */
include '../bd/update-image.php';
session_start();
// print_r($_FILES ['file']);
$directorio = __DIR__.'/images/';
$subir_archivo = $directorio.basename($_FILES['file']['name']);
$type = $_FILES['file']['type'];
$size = $_FILES['file']['size'];
if (!((strpos($type, "gif") || strpos($type, "jpeg") ||  strpos($type, "jpg") ||  strpos($type, "png")) && ($type < 100000)))
{
    echo "Este archivo es muy grande o no se trata de una imagen ";
}else
{
    if (move_uploaded_file($_FILES['file']['tmp_name'], $subir_archivo))
    {
        $imageContentBinary = file_get_contents($subir_archivo);
        $imagenComoBase64 = base64_encode($imageContentBinary);
        echo $imagenComoBase64;
    }else
    {
        echo 0;
    }
}

?>