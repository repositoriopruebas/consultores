<?php
include "connect.php";
include "../class/PostMeme.php";
if(isset($_GET['categories']))
{
    queryCategories();
}
if(isset($_GET['postsbycategory']))
{
    postByCategory($_GET['postsbycategory']);
}

function queryCategories()
{
    $mysql = connect();
    $arrayCategories = [];
    $sql = "SELECT * FROM categories";
    if($res = $mysql->query($sql))
    {
        while($row = $res->fetch_assoc())
        {
            $categories = new stdClass();
            $categories->id = $row['id'];
            $categories->category = $row['category'];
            array_push($arrayCategories,$categories);


        }
    }
    echo json_encode($arrayCategories);
}
function postByCategory($idcategory)
{
    $mysqli = connect();
    $postbycategory = [];
    $sql = "SELECT * FROM myposts WHERE idcategory = $idcategory  ORDER BY fecha DESC";
    if($res = $mysqli->query($sql))
    {
        while($row = $res->fetch_array())
        {
            $Posts = new PostMeme();
            $Posts->id = $row['id'];
            $Posts->info =  $row['info'];
            $Posts->image = $row['image'];
            $Posts->date = $row['fecha'];
            $Posts->category = $row['idcategory'];
            $Posts->likes = $row['likes'];
            $Posts->altimage = $row['alt'];
            $Posts->captionimage = $row['figurecaption'];
            $Posts->imagedescription = $row['imagedescription'];
            array_push($postbycategory, $Posts);

        }
        echo json_encode($postbycategory);
    }

}
?>