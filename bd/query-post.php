<?php
include 'connect.php';
include '../class/PostMeme.php';
if(isset($_POST['allpost']))
{
    allPosts();
}
if(isset($_POST['idpost']))
{
    postByTag($_POST['idpost']);
}
if(isset($_POST['latests']))
{
    lastPosts();
}
if(isset($_POST['top']))
{
    queryTop();
}
function allPosts()
{
    $mysqli = connect();
    $postArray = [];
    $sql = "SELECT * FROM myposts ORDER BY fecha DESC ";
    if($res = $mysqli->query($sql))
    {
        while($row = $res->fetch_assoc())
        {
            $Posts = new PostMeme();
            $Posts->id = $row['id'];
            $Posts->info =  $row['info'];
            $Posts->image = $row['image'];
            $Posts->date = $row['fecha'];
            $Posts->category = $row['idcategory'];
            $Posts->likes = $row['likes'];
            $Posts->altimage = $row['alt'];
            $Posts->captionimage = $row['figurecaption'];
            $Posts->imagedescription = $row['imagedescription'];
            array_push($postArray,$Posts);
        }
    }
    echo  json_encode($postArray);
}
function lastPosts()
{
    $mysqli = connect();
    $postArray = [];
    $sql = "SELECT * FROM myposts ORDER BY fecha DESC LIMIT 5";
    if($res = $mysqli->query($sql))
    {
        while($row = $res->fetch_assoc())
        {
            $Posts = new PostMeme();
            $Posts->id = $row['id'];
            $Posts->info =  $row['info'];
            $Posts->image = $row['image'];
            $Posts->date = $row['fecha'];
            $Posts->category = $row['idcategory'];
            $Posts->likes = $row['likes'];
            array_push($postArray,$Posts);
        }
    }
    echo  json_encode($postArray);
}
function queryTop()
{
    $mysql = connect();
    $postArray = [];
    $sql = "SELECT * FROM myposts ORDER BY likes DESC LIMIT 5";
    if($res = $mysql->query($sql))
    {
        while($row = $res->fetch_assoc())
        {
            $Posts = new PostMeme();
            $Posts->id = $row['id'];
            $Posts->info =  $row['info'];
            $Posts->image = $row['image'];
            $Posts->date = $row['fecha'];
            $Posts->category = $row['idcategory'];
            $Posts->likes = $row['likes'];
            array_push($postArray,$Posts);
        }
    }
    echo json_encode($postArray);
}
function postByTag($id)
{
    $mysqli = connect();
    $sql = "SELECT * FROM myposts WHERE id=$id ORDER BY fecha DESC ";
    if($res = $mysqli->query($sql))
    {
        while($row = $res->fetch_assoc())
        {
            $Posts = new PostMeme();
            $Posts->id = $row['id'];
            $Posts->info =  $row['info'];
            $Posts->image = $row['image'];
            $Posts->date = $row['fecha'];
            $Posts->category = $row['idcategory'];
            $Posts->likes = $row['likes'];
            $Posts->altimage = $row['alt'];
            $Posts->captionimage = $row['figurecaption'];
            $Posts->imagedescription = $row['imagedescription'];
            echo json_encode($Posts);
        }
    }
}

?>