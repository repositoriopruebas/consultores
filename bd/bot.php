<?php

include 'connect.php';
include ' __DIR__."/../class/PostMeme.php';
$var = '';
$name = '';
//$image = '';
$url = '';
if(is_bot())
{
    $var = 'Imagenes para compartir por redes sociales';
    $mysqli = connect();
    $postArray = [];
    $sql = "SELECT * FROM myposts ORDER BY fecha DESC ";
    if($res = $mysqli->query($sql))
    {
        while($row = $res->fetch_assoc())
        {
            $Posts = new PostMeme();
            $Posts->id = $row['id'];
            $Posts->info =  $row['info'];
            $Posts->image = $row['image'];
            $Posts->date = $row['fecha'];
            $Posts->category = $row['idcategory'];
            $Posts->likes = $row['likes'];
            array_push($postArray,$Posts);
        }
    }
    foreach($postArray as $post)
    {
        $url = str_replace(" ","_",strtolower(trim($post->info)));
        $name .= "<a href='/post-$post->id/".str_replace(" ","_",$url).".html'>".$post->info."</a>";
        //$image .= "<img src='/admin/images/".$url.".jpg'>";
    }
}else
{
    $var = "Imagenes para compartir por redes sociales";
}
function is_bot(){

    $bots = array(
        'Googlebot', 'Baiduspider', 'ia_archiver',
        'R6_FeedFetcher', 'NetcraftSurveyAgent', 'Sogou web spider',
        'bingbot', 'Yahoo! Slurp', 'facebookexternalhit', 'PrintfulBot',
        'msnbot', 'Twitterbot', 'UnwindFetchor',
        'urlresolver', 'Butterfly', 'TweetmemeBot' );

    foreach($bots as $b){
        if( stripos( $_SERVER['HTTP_USER_AGENT'], $b ) !== false ) return true;

    }
    return false;
}
function allPostsPreRendering()
{
    $mysqli = connect();
    $postArray = [];
    $sql = "SELECT * FROM myposts ORDER BY fecha DESC ";
    if($res = $mysqli->query($sql))
    {
        while($row = $res->fetch_assoc())
        {
            $Posts = new PostMeme();
            $Posts->id = $row['id'];
            $Posts->info =  $row['info'];
            $Posts->image = $row['image'];
            $Posts->date = $row['fecha'];
            $Posts->category = $row['idcategory'];
            $Posts->likes = $row['likes'];
            array_push($postArray,$Posts);
        }
    }
    return  $postArray;
}
?>