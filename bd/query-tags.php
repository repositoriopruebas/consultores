<?php
include 'connect.php';
if(isset($_GET['querytags']))
{
    queryTags();
}
function queryTags()
{
    $mysqli = connect();
    $sql = 'SELECT * FROM mytags ORDER BY tag ASC';
    $array = [];
    if($res = $mysqli->query($sql))
    {
        while($row = $res->fetch_object())
        {
            array_push($array,$row);
        }
        echo json_encode($array);
    }else
    {
        echo "Error al ejecutar la query : ".$mysqli->error;
    }
}
