<?php
include 'class/user.php';
include 'php/setogmetatags.php';
//include 'bd/bot.php';
//https://search.google.com/search-console?resource_id=https%3A%2F%2Fmenudosgift.es%2F
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- jQuery -->
    <meta charset="UTF-8">
    <!-- Global site tag (gtag.js) - Google Analytics -->
  <!--  <script async src="https://www.googletagmanager.com/gtag/js?id=G-7E39ZCY1CK"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-7E39ZCY1CK');
    </script>
    <meta name="google-site-verification" content="dHJVrBpR-4WnYlkfk1iaS-dg4HplHjN6dK0-LRxYbqc" />-->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta
            name="description"
            content="<?php echo $params['description']; ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="keywords" content="imagenes,memes,humor,gifts,animados,amor"/>
    <!--Facebook y Whatsapp-->
    <meta property="og:title" content="<?php echo $params['title']; ?>"/>
    <meta property="og:url" content="" />
    <meta property="og:type" content="<?php echo $params['type']; ?>" />
    <meta property="og:image" content="<?php echo $params['image']; ?>"/>
    <meta property="og:description" content="<?php echo $params['description']; ?>" />
    <meta property="fb:app_id" content="1706547359638528"/>
    <!--Twitter-->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@Francis18811582" />
    <meta name="twitter:title" content="<?php echo $params['title'] ?>" />
    <meta name="twitter:description" content="<?php echo $params['description']; ?>" />
    <meta name="twitter:image" content="<?php echo $params['image']; ?>" />
  <!-- MDB icon -->
  <!-- Font Awesome -->
<!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">-->
  <!-- Google Fonts Roboto -->
 <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">-->
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="/css/mdb.min.css">
  <!-- Your custom styles (optional) -->
 <!-- <link rel="stylesheet" href="/css/style.css">-->
  <link rel="stylesheet" href="/css/estilos.min.css">
  <style>
      .view {
          height: 100%;
      }
      .user p{
          color:white;
          background-color: white;
      }
      .container {
          padding: 2rem 0rem;
      }
      .modal-dialog {
          margin-top: 0rem;
          max-width: 100%;
      .modal-content {
          border-radius: 0rem;
      }
      .buttons {
      .btn {
          margin: 0.2rem;
      }
      }
      }
      .section a{
          color:#e0e0e0;
      }
  </style>
  <title>Menudos Gift humor y amor para todo el mundo</title>
</head>
<body itemscope itemtype="http://schema.org/WebPage" style="font-display:swap; font-family: Arial, Helvetica, Verdana, sans-serif; overflow-x: hidden">
<section id="carga"  class="blue-gradient" style="display:none;height:1800px; width:100%;">

</section>
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar">
      <div class="container-fluid">
        <a class="navbar-brand bg" href="/index.php">
         <h1 class="h3 mt-2">
            <strong>
                Menudos GIFT
            </strong>
             <small class="text-hide">Humor y amor para todo el mundo</small>
         </h1>
            <strong> <a href="/php/login.php" class="btn btn-block btn-social btn-google " id="Google"><span class="fab fa-google text-white"><strong>Entrar con Google</strong></span></a></strong>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
          <ul id="list-menu" class="navbar-nav mr-auto mt-1">
            <li class="nav-item ">
          </ul>
        </div>
       <div id="searching" class="text-light">
           <span>....</span>
       </div>
          <!--<form class="form-inline d-flex justify-content-center md-form form-sm active-cyan-2 mt-2">
              <input id="searching" required class="form-control form-control-sm mr-3 w-75 text-light" type="text" placeholder="Evitar inyección de código"
                     aria-label="Search">
              <a id="link-search" class="fas fa-search text-light"   data-value=""></a>
          </form>-->
          <div  id="dropdown" class="dropdown bg-dark">
              <button class="btn btn-sm btn-outline-white btn-rounded wow fadeInLeft " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="img-fluid img-thumbnail  rounded-circle card-img-64 mr-2" id="profile-header" src="" alt=""> <span id="user-name"></span>
                <i class="fas fa-angle-down ml-3"></i>
              </button>
              <div id="dropdown-menu" class="dropdown-menu">
                <a class="dropdown-item" id="close-session" href="#">Cerrar Sessión  </a>
              </div>
          </div>
        </div>
    </nav>
    <div id="tagsearch" style="margin-top: 8%;  position:fixed;z-index:100">
      <a class="badge badge-success seetag"><span style="font-size: 18px;">Buscar temas</span></a>
    </div>
      <section style="" class="container deep-blue-gradient seccion " >
          <button style="width:400px; position:absolute;top:0px;left:160px" type="button " class="close btn-block w-100" data-dismiss="modal" aria-label="Close">
            &times;
          </button>
      </section>
      <div style="gap:10px;min-height: 96vh; margin-top: 25%; " class="contenido mt-5 mb-5 ">
          <div style="box-sizing: border-box;width:20%" class=" tags p-sm-1 sidebar">
              <h2 class="badge badge-light latests">Lo último de Menudo GIF</h2>
          </div>
          <div style="min-height: 60vh" id="view-post" class="bg-light p-4 w-responsive">
              <div id="tag" class="d-flex flex-row justify-content-center">
              </div>
              <?php //if($name ) echo $name ?>
          </div>
          <div  style="box-sizing: border-box;width:20%" class="top p-sm-1">
              <h2 id="bests-title" class="lead w-10 badge badge-light p-sm-1 ">Los más votados </h2>
          </div>
      </div>
  </div>

  <!-- Footer -->
<footer style="position:relative;bottom:-32px;z-index:-4;" class="w-100 h-10 page-footer font-small mt-5">
  <!-- Footer Elements -->
  <div class="container">
    <!-- Grid row-->
    <div class="row">
      <!-- Grid column -->
      <div class="col-md-12">
        <div class="flex-center">
            <span class="lead text-center">Menudos Gift Humor y amor para todo el mundo 2021 © El Balcón</span>
        </div>
      </div>
      <!-- Grid column -->
    </div>
    <!-- Grid row-->
  </div>
  <!-- Copyright -->
</footer>
  <div class="modal fade" id="cookieModal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-body">
                  <div class="notice d-flex justify-content-between align-items-center">
                      <div class="cookie-text">Utilizamos cookies para mejorar la experiencia del usuario</div>
                      <div class="buttons d-flex flex-column flex-lg-row">
                          <a href="#a" class="btn  btn-success btn-sm accept" data-dismiss="modal">Aceptar</a>
                          <a href="#a" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
<div  class="modal fade right" id="fullHeightModalTop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
      aria-hidden="true">
    <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
    <div class="modal-dialog modal-full-height modal-top" role="document">
        <div class="modal-content">
            <input type="hidden" id="idoculto">
            <div class="modal-header d-flex flex-row align-items-around ">
                <h4 class="modal-title w-100 ml-4" id="myModalLabel">Error de entrada</h4>
                <ul id="profile-link-nav" style="list-style: none" class="d-flex flex-row align-items-center justify-content-around w-100 mt-3 lead navbar-nav mr-auto row">
                    <li><a class="default-link" id="profile-link-image" data-value="formulary">Sube tu imagen de perfil</a></li>
                    <li><a id="profile-link-quote" data-value="text-area">Descripción</a></li>
                    <li><a id="profile-link-post"  data-value="post">Escribe tu comentario</a></li>
                    <li><a id="profile-link-edit"  data-value="delete">Borrar registros</a></li>
                    <li><a id="profile-link-edit-post"  data-value="edit">Editar registros</a></li>
                </ul>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body ">
                <div class="row d-flex justify-content-around">
                    <div class="col-lg-4 col-md-12 mb-4">
                        <!--Card-->
                        <div class="card testimonial-card">
                            <!--Background color-->
                            <div class="card-up teal lighten-2">
                            </div>
                            <!--Avatar-->
                          <!--  <div class="avatar mx-auto white text-center image">
                                <img width="40%" id="profile-img" class="img-fluid rounded" src="" alt="">
                            </div>-->
                            <div id="info-profile" class="card-body">
                                <!--Name-->
                                <h5 id="name-profile" class="card-title mt-1 text-center"></h5>
                                <h5 id="email-profile" class="card-title mt-1 text-center"></h5>
                                <hr>
                                <!--Quotation-->
                                <p><i class="fas fa-quote-left"></i>Pruebe otra manera más adecuada para entrar a su administrador.</p>

                            </div>
                        </div>
                        <!--Card-->
                    </div>
                    <!--Grid column-->
                    <div id="components-profile" class="d-flex flex-row justify-content-center">
                        <div class=" form-profile form-group">

                        </div>
                        <div id="profile-description" class="quote-profile md-form secondary-textarea active-secondary-textarea-2">
                            <form style="display: none" id="text-area">

                                <textarea class="md-textarea  form-control text-dark" onkeypress="countChars()" name="text" id="quote" cols="38" rows="6" placeholder="Añade una breve descripción de ti"></textarea>
                                <button onclick='buttonQuote()' class="btn btn-sm btn-outline-secondary w-100 waves-effect" type="button" id="boton">enviar</button>
                                <label id="tags" for=""></label>
                                <p id="countchars"></p>
                            </form>
                        </div>
                        <div class= "grid form-group d-flex flex-row justify-content-center quote-profile md-form secondary-textarea active-secondary-textarea-2">
                            <form  id="post">
                                <div class="row">
                                    <div id="panelcategory" class="mb-5 col-md-3">

                                    </div>
                                    <div id="categories" class="col-md-3">

                                    </div>
                                    <div class="col-md-3">
                                        <textarea   class="md-textarea form-control text-dark" name="profile-post" id="posts" cols="38" rows="6"></textarea>
                                        <div class="btn-group">
                                            <button  type="button" id="submit-post-button" class="btn btn-sm btn-outline-secondary ">Publicar </button>
                                            <button  type="button" id="submit-createtag-button" class="btn btn-sm btn-outline-secondary">Ver tags </button>
                                        </div>
                                    </div>
                                    <div id="content-image" class="col-md-3 btn-group">
                                        <textarea id = "alt-image" rows = "4" cols = "36"
                                                  placeholder = "rellenar alt de la imagen"
                                                  class = "valid"></textarea>
                                        <textarea id = "caption-image" rows = "4" cols = "36"
                                                  placeholder = "Rellenar descripción general"
                                                  class = "valid"></textarea>
                                        <textarea id = "description-image" rows = "4" cols = "36"
                                                  placeholder = "rellenar descripción de la imagen "
                                                  class = "valid"></textarea>
                                    </div>
                                </div>
                                <!-- <button type="button" id="post-button" class="btn btn-sm btn-outline-secondary w-100 waves-effect">Listado etiquetas clave</button>-->
                                <!--<button  type="button" id="submit-viewquery-button" class="btn btn-sm btn-outline-secondary  waves-effect">Ver entradas </button>-->
                                <p style="display:block;" class="lead labeltags mt-3 mr-4">Etiquetas a elegir</p><br>
                            </form>

                            <form  id="img-post" class="text-center  p-3"  action="#" method="POST" enctype="multipart/form-data">
                                <input class="btn btn-sm btn-outline-secondary waves-effect" type="file" id="file-post-image">
                                <button class="btn btn-sm btn-outline-secondary waves-effect" type="button" id="send-image-post">Enviar</button>
                            </form>
                        </div>
                        <div class="w-33 form-group">
                            <form id="delete" >
                            </form>
                        </div>
                        <div class="w-33 form-group">
                            <form id="edit" class="d-flex flex-row flex-wrap" >
                            </form>
                            <div class="card view-edit-tags">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
  </div>
<!-- Footer -->
  <script  type="text/javascript" src="/js/jquery.min.js"></script>
  <script async src="/js/eventuser.js"> </script>
  <script defer type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript">
    var scrolltotop={setting:{startline:100,scrollto:0,scrollduration:1e3,fadeduration:[500,100]},controlHTML:'<img src="https://i1155.photobucket.com/albums/p559/scrolltotop/arrow3.png" />',controlattrs:{offsetx:5,offsety:5},anchorkeyword:"#top",state:{isvisible:!1,shouldvisible:!1},scrollup:function(){this.cssfixedsupport||this.$control.css({opacity:0});var t=isNaN(this.setting.scrollto)?this.setting.scrollto:parseInt(this.setting.scrollto);t="string"==typeof t&&1==jQuery("#"+t).length?jQuery("#"+t).offset().top:0,this.$body.animate({scrollTop:t},this.setting.scrollduration)},keepfixed:function(){var t=jQuery(window),o=t.scrollLeft()+t.width()-this.$control.width()-this.controlattrs.offsetx,s=t.scrollTop()+t.height()-this.$control.height()-this.controlattrs.offsety;this.$control.css({left:o+"px",top:s+"px"})},togglecontrol:function(){var t=jQuery(window).scrollTop();this.cssfixedsupport||this.keepfixed(),this.state.shouldvisible=t>=this.setting.startline?!0:!1,this.state.shouldvisible&&!this.state.isvisible?(this.$control.stop().animate({opacity:1},this.setting.fadeduration[0]),this.state.isvisible=!0):0==this.state.shouldvisible&&this.state.isvisible&&(this.$control.stop().animate({opacity:0},this.setting.fadeduration[1]),this.state.isvisible=!1)},init:function(){jQuery(document).ready(function(t){var o=scrolltotop,s=document.all;o.cssfixedsupport=!s||s&&"CSS1Compat"==document.compatMode&&window.XMLHttpRequest,o.$body=t(window.opera?"CSS1Compat"==document.compatMode?"html":"body":"html,body"),o.$control=t('<div id="topcontrol">'+o.controlHTML+"</div>").css({position:o.cssfixedsupport?"fixed":"absolute",bottom:o.controlattrs.offsety,right:o.controlattrs.offsetx,opacity:0,cursor:"pointer"}).attr({title:"Scroll to Top"}).click(function(){return o.scrollup(),!1}).appendTo("body"),document.all&&!window.XMLHttpRequest&&""!=o.$control.text()&&o.$control.css({width:o.$control.width()}),o.togglecontrol(),t('a[href="'+o.anchorkeyword+'"]').click(function(){return o.scrollup(),!1}),t(window).bind("scroll resize",function(t){o.togglecontrol()})})}};scrolltotop.init();</script>
<noscript>Not seeing a <a href="https://www.scrolltotop.com/">Scroll to Top Button</a>? Go to our FAQ page for more info.</noscript>
</body>
</html>
