var linksToActive = [];
var componentsToActive = [];
var modalLinks = '';
var responseJsonTags = [];
var tagsArray = [];
var resultBase64Post = '';
var category = '';
var categories = '';
var cont = 0;
var togglemodal = 'show';
var countTags = 0;
const title = ' Memes de humor y de amor  para todo el mundo  |';
const description = 'MenudosGift contiene para ti los memes más  originales  en español para que puedas compartir con todo el mundo';
const titleTags = 'Humor original, amor, amistad y frases famosas para todo el mundo , imagen de ||';
const descriptionTags = 'Tu sitio con los memes  mas originales está aquí';
const tagsPaginate = 80;
function checkSession()
{
    recoveryUserSession().then(object =>
    {

        if (object == 0)
        {
            buttonOff();
            $('#fullHeightModalTop').css('display','none');
            if($('#fullHeightModalTop'))
            {
                $('#fullHeightModalTop').empty();
            }
        }else
        {
            save(object);
        }
    });
}
function save(userSession)
{
    $.post('/../bd/insert-user.php',{'resp':userSession},function(data){
        dataclean = JSON.parse(JSON.stringify(data));
        userSession.id = dataclean.id;
        $('#user-name').text(userSession.name);
        $('#dropdownMenuButton, #profile-header').attr('src','data:image/png;base64,'+userSession.avatar);
        $('#text-box').css('display','block');
        $('#list-menu').append(editUser(userSession));
        $.get('php/insertidsession.php',{'sessionuser':userSession.iduser},function(res){

        });

    });
}
function editUser(userSession)
{
    var component = '<li class="nav-item">'+
        //  '<a class="nav-link" href="admin/admin.php?iduser='+id+'">Edit User</a>'+
        '<a onclick="modalController()" class="nav-link" id="profile" data-value="'+userSession.iduser+'">Edit User</a>'+
        '</li>';
    return component;
}
function editUseyr()
{
    var component = '<li class="nav-item">'+
        //  '<a class="nav-link" href="admin/admin.php?iduser='+id+'">Edit User</a>'+
        '<a onclick="modalController()" class="nav-link" id="profile" >Edit User</a>'+
        '</li>';
    return component;
}
function modalController()
{
    recoveryUserSession().then(object => (object != 0)? showModal(object):'');
}
function setComponents()
{
    $('#components-profile').children().each(function(index,value){
        if($(value).find('form').attr('id'))
        {
            componentsToActive.push($(value).find('form').attr('id'));
        }
    });
}
function setLinksId()
{
    $('#profile-link-nav').children().each(function(index){
        LinksObject($(this).find('a').attr('id'),($(this).find('a').data('value')));
    });
}
function LinksObject(id,linkvalue)
{
    var linkInfo = {
        "id":id,
        "linkvalue":linkvalue
    }
    linksToActive.push(linkInfo);
}
function getLinksId(idhtml)
{
    var id = idhtml;

    $.each(linksToActive,function(index,value){
        if(value.id == id)
        {
            $('#'+value.id).css('color','red');
            getEnabledComponents(value.linkvalue);
        }else
        {
            $('#'+value.id).css('color','black');
            getDisabledComponents(value.linkvalue);
        }
    });
}
function getDisabledComponents(linksearch)
{
    $.each(componentsToActive,function(index,value){
        if(linksearch == value)
        {
            //$('#'+value).children().css('display','none');
            $('#'+value).css('display','none');
        }
    });
}
function getEnabledComponents(linksearch)
{
    if(linksearch == 'post')
    {
        $('#img-post').show();

    }else
    {
        $('#img-post').hide();
    }
    for(let i=0; i<componentsToActive.length;i++)
    {
        if(componentsToActive[i] == linksearch)
        {
            $('#'+componentsToActive[i]).css('display','block');
        }
    }
};
function queryCategories()
{
    return new Promise((resolve,reject) => {
        $.get('../bd/query-categories.php',{"categories":"categories"},function (response)
        {
            resolve(response);
        })
    });
}

function renderCategories(arrayCategories)
{
    $('#categories').empty();
    $('#categories').text("categoria")
    $.each(arrayCategories,function(index,value){
        $('#categories').append('<a onclick="doCategory(this)" class="mr-4 mb-5" data-value="'+value.id+'"><span class="badge badge-pill badge-danger p-2">'+value.category+'</span></a>');
    });
}
function doCategory(el)
{
    category = $(el).data("value");
    categories = '<span>Categoría escogida</span><span class="badge badge-pill badge-success">'+$(el).find('span').text()+'</span>';
    $('#panelcategory').html(categories);
}
function recoveryTags()
{
    var array = [];
    $.get('../bd/query-tags.php',{"querytags":1},function(response){
        responseJsonTags = JSON.parse(response);
        // console.log(responseJsonTags);
        var tag = '';
        /* $.each(responseJsonTags,function(index,value){
            tag = value.tag;

            tagsTryFirst(value.tag,value.id);
        });*/
    });
}
function initModalLink()
{
    setLinksId();
    setComponents()
    $('#profile-link-image').css('color','red');
    $('#text-area').css('display','none');
    $('#caque').css('display','none');
    $('#size').css('display','none');
    $('#formulary').css('display','block');
}
function showModal(userModal)
{
    if(userModal)
    {
        $('#categories')
        getDisabledComponents('text-area');
        $('#myModalLabel, #name-profile').text(userModal.name);
        $('#email-profile').text(userModal.email);
        $('#profile-img').attr('src','data:image/png;base64,'+userModal.avatar);
        $('#idoculto').val(userModal.iduser);
        $('#info-profile').text(userModal.quote);
        $('.form-profile').html(
            '<form  style="display:none" id="formulary" class="text-center  p-3"  action="#" method="POST" enctype="multipart/form-data">'+
            '<label >Sube una imagen para tu avatar</label><br>'+
            '<input class="btn btn-sm btn-outline-secondary waves-effect" require type="file"  name="file" id="file">'+
            '<button class="btn btn-sm btn-outline-secondary waves-effect"  onclick="submitAjaxImage()" type="button" id="send">Enviar</button>'+
            '</form>'
        );
        initModalLink();
    }
    $('#fullHeightModalTop').modal('show');
    checkLinksOneRed();
    $('#submit-viewquery-button').hide();
}
function checkLinksOneRed()
{
    $('#profile-link-nav').children().children().each(function(index,value){
        if($(value).attr('id') != 'profile-link-image' )
        {
            $(value).css({'color':'black'});
            $('#'+$(value).data('value')).hide();
            $('#img-post').hide();
        }
    });
}
function submitAjaxImage()
{
    if($('#file')[0].files[0] && $('#idoculto').val())
    {
        var formData = new FormData();
        var files = $('#file')[0].files[0];
        formData.append('file',files);
        recoveryUserSession().then(object =>
            $.ajax({
                url:'../admin/imageprocess.php',
                type:'post',
                data:formData,
                contentType:false,
                processData:false,
                success:function(response)
                {
                    $('#profile-img').attr('src','data:image/png;base64,'+response);
                    $('#dropdownMenuButton, #profile-header').attr('src','data:image/png;base64,'+response);
                }
            })
        );
    }else
    {
        alert("Logueate o introduce un archivo");
    }
}
function imagePostUpload()
{
    if($('#file-post-image')[0].files[0])
    {
        var formData = new FormData();
        var files = $('#file-post-image')[0].files[0];
        formData.append('file',files);
        responseImageProcessPost(formData).then(o => {
            resultBase64Post = o;
        })
    }else
    {
        alert("no hello");
    }
}
function responseImageProcessPost(formData)
{
    return new Promise((resolve,reject)=>{
        $.ajax({
            url:'../admin/imageprocesspost.php',
            type:'post',
            data:formData,
            contentType:false,
            processData:false,
            success:function(response)
            {
                resolve(response)
            }
        })
    })
}
function countChars()
{
    var maxchar = 200;
    var divchar = maxchar - $('#quote').val().length;
    if(divchar<1)
    {
        $('#countchars').text("Número máximo de carácteres permitido");
    }else
    {
        $('#countchars').text("Número caracteres disponibles: "+divchar);
    }
}
function recoveryUserSession()
{
    var result = '';
    return new Promise ((resolve,reject)=>{
        $.get('/php/checksession.php',{'checksession':true},(data) => {
            resolve(JSON.parse(data));
        });
    });
}
function buttonOff()
{
    $('#dropdownMenuButton').remove();
    $('#dropdown i').remove('display','none');
    $('#dropdown button').remove();
}
function buttonOn()
{
    $('#dropdown-menu').css('display','block');
    $('#dropdown i').css('display','block');

}
function insertQuote(user)
{
    if($('#quote').val() )
    {
        user.quote = $('#quote').val();
        $.post('../bd/update-image.php',{'userid':user.iduser,'userquote':user.quote},function(response){
            if(response == 0)
            {
                $.get('php/insertidsession.php',{'sessionuser':response},function(response)
                {
                    showModal(JSON.parse(response));
                });
            }
        });
    }
}
function searchByTag()
{
    var cont = 0;
    var objectJSON = {idtag:"",tags:""};
    $.each(responseJsonTags,function(index,value){
        if ($('#searching').val() == value.tag)
        {
            objectJSON.idtag=value.id;
            objectJSON.tags = value.tag;
        }
    });
    /* if(objectJSON.idtag > 0)
    {

       $('#view-post').empty();

       queryPostByTag(objectJSON.idtag).then(idpost =>{
           var idpostSp = idpost.split(" ");
           $.each(idpostSp,function(index,value){
               if(value != '')
               {

                   queryPostById(value);
               }
           });
       });
    }else
    {
       alert("la palabra no está");
    }*/
    return objectJSON
}
function buttonQuote()
{
    recoveryUserSession().then(object => insertQuote(object)
    );
    $('.form-profile').css('display','block');
    $('#profile-link-quote').css('color','black');
}
function deleteLastTags()
{
    $('.tags').remove();
    $('#post span').remove();
    $('.value').empty();
    $('#title').empty();

}
function tagsTryFirst(words)
{
    var worldsSeparate = words.split(' ');
    var filterwords = tagsRepeat(worldsSeparate);
    var resultWords = compareTagsQueryWithTextArea(filterwords);
    var mergeAllTags = insertNewTags(resultWords);
    $('.labeltags').show();
    $.each(mergeAllTags,function(index,value) {
        if(value.tag.length > 2)
        {
            $('#post').append('<a onclick="clicked(this)" class="tags" style="color" id="'+value.tag+'" data-value="'+value.id+'"><span class="value badge badge-secondary m-1">'+value.tag+'</span></a>'
            );
        }
    });
    $('#post').append('<br><input id="maketag" type="text" placeholder="introduce tu propia etiqueta"><button class="btn btn-sm btn-mdb-color" type="button" id="sendtag" value="" onclick="makeTag()">Enviar tag</button>');
}
function makeTag() {
    // var filterwords = tagsRepeat($('#maketag').val());
    responseJsonTags.push({"id":"1","tag":$('#maketag').val()});
    var filterwords = tagsRepeat(responseJsonTags);
    var resultWords = compareTagsQueryWithTextArea(filterwords);
    var mergeAllTags = insertNewTags(resultWords);
    $('.tags').remove();
    $('#maketag').remove();
    $('#sendtag').remove();
    $('.labeltags').show();
    $.each(mergeAllTags,function(index,value) {
        if(value.tag.length > 2)
        {
            $('#post').append('<a onclick="clicked(this)" class="tags" style="color" id="'+value.tag+'" data-value="'+value.id+'"><span class="value badge badge-secondary m-1">'+value.tag+'</span></a>'
            );
        }
    });
    $('#post').append('<br><input id="maketag" type="text" placeholder="introduce tu propia etiqueta"><button class="btn btn-sm btn-mdb-color" type="button" id="sendtag" value="" onclick="makeTag()">Enviar tag</button>');
}
function clicked(linka)
{
    if ($(linka).hasClass("text-secondary"))
    {
        $(linka).removeClass('add');
        $(linka).removeClass("text-secondary");
        $(linka).css('opacity','1');
        iterateRemoveTag($(linka).data('value'));
    }else
    {
        $(linka).addClass("text-secondary");
        $(linka).addClass('add');
        $(linka).css('opacity','0.6');
    }
}
function iterateRemoveTag(idtag)
{
    for(let i = 0; i<responseJsonTags.length;i++)
    {
        if(responseJsonTags[i].id == idtag)
        {
            responseJsonTags.splice(i,1);
        }
    }
}

function compareTagsQueryWithTextArea(filterwords)
{
    var bok = false;
    var elementFilter = '';
    for(let i=0; i<filterwords.length;i++)
    {
        for(let x=0; x<responseJsonTags.length;x++)
        {
            if(filterwords[i] == responseJsonTags[x].tag)
            {
                bok = true;
                if(filterwords.splice(i,1))
                {
                    console.log("borrado ");
                }
            }
        }
    }
    return filterwords;
}
function insertNewTags(resultWords)
{
    for(let i= 0; i<resultWords.length;i++)
    {
        responseJsonTags.push({id:"1",tag:resultWords[i]});
    }
    return responseJsonTags;
}
function showButton()
{
    var elementsCheckBox = $('input.tags');
    var checkElement = elementsCheckBox.toArray().find(function(elemento){
        return $(elemento).attr('checked');
    });
}
function submitTags()
{
    deleteLastTags();
    // $('#submit-tag-button').removeAttr('disabled');
    tagsTryFirst($('#posts').val());
    $('#submit-post-button').show();
    $('#submit-tag-button').show();
}
function tagsRepeat(tagsArray)
{
    let hash = {};
    tagsArray = tagsArray.filter(o => hash[o] ? false : hash[o] = true);
    return tagsArray;
}
function submitInsertPost(iduser)
{

    $('#post a').each(function(){
        var add = $(this).attr('class').split(" ");
        if(add[add.length-1] == 'add')
        {
            tagsArray.push({tags:$(this).attr('id'),id:$(this).data('value')});
        }
    });
    var userpost = {"id":iduser,"checkbox":tagsArray,"info":$('#posts').val(),"picture":(resultBase64Post)?resultBase64Post:alert("te falta meter la imagen cabezón"),"category":category,"alt":$('#alt-image').val(),"figurecaption":$('#caption-image').val(),"imagedescription":$('#description-image').val()};
    if(tagsArray.length != 0)
    {
        insertPost(userpost).then(o => {
            alert(o);
            window.location.reload();
            $('#submit-viewquery-button').show();
            $('#submit-post-button').attr('disabled','true');
            $('#posts').val('');
            $('.tags').hide();
            tagsArray = [];
        });

    }else
    {
        alert("Tiene que pulsar en el botón etiqueta y elegir como mínimo una");
    }
}
function insertPost(userpost)
{
    return new Promise((resolve,reject) => {
        $.post('bd/insert-post.php',{"post":userpost},function(response){
            if(response)
            {
                resolve(response);
            }else
            {
                reject(response);
            }
        });
    });
}
function disabledButtonTag()
{
    $('#submit-createtag-button').attr('disabled','true');
}
function enabledButtonTag()
{
    $('#submit-createtag-button').removeAttr('disabled');
}
function queryPosts()
{
    $.ajax({
        type: 'POST',
        url: '../bd/query-post.php',
        data: {"allpost":"all"},
        beforeSend: function() {
            $('#carga').show();
            $('#loading').removeClass('spinner-border');
        },
        success: function(response) {

            var PostParsing =  JSON.parse(response);
            var objectPost = {};
            $.each(PostParsing,function(index,value){
                var tagview = '';
                objectPost = {"post":value};
                renderController(objectPost);
            });
            $('#carga').hide();
        },
        error: function(xhr) { // if error occured
            alert("Error occured.please try again");
            $(placeholder).append(xhr.statusText + xhr.responseText);
            $(placeholder).removeClass('loading');
        },
        complete: function() {

            $('#view-post').removeClass('class','bg-white');
            $('#view-post').attr('class','bg-light');
        },
// dataType: 'html'
    });
}
function queryPostById(idPost,bol)
{
    $.post('../bd/query-post.php',{"idpost":idPost},function(response){
        var respJSONPost = JSON.parse(response);
        queryPostTag(respJSONPost.id).then(idtag => {
            var array = idtag.split(" ");
            var  objectPost = {"post":JSON.parse(response),"idtag":array};
            if(!bol)
            {
                renderController(objectPost);
               // dynamicAlt(objectPost,objectPost.post.category)
            }else
            {
                queryTagsByPost(objectPost);
                //dynamicAlt(objectPost,objectPost.post.category)
            }
        });
    });

}
function queryPostTag(idpost)
{
    return new Promise ((resolve,reject)=>{
        $.post('../bd/query-post-tag.php',{"idpost":idpost},function(response){
            resolve(response);
        });
    })
}
function queryTagsByPost(objectPost) {
    var array = [];
    for(let i = 0; i<objectPost.idtag.length; i++)
    {
        $.ajax({
            async:false,
            type:'post',
            url:'../bd/query-post-tag.php',
            data:{
                'idtag':objectPost.idtag[i]
            },
            success:function(data)
            {
                var objectResponse = {'idtag':objectPost.idtag[i],"tags":data}
                array.push(objectResponse);
            }
        });
    }
    $('.containertags').empty();
    var object = {"post":objectPost.post,"array":array};
    renderController(object);
}
//d-flex flex-column justify-content-center align-items-center  mt-5 entry
function renderController(object)
{
    $('#view-post').append(openContainer());
    $('#view-post').append(renderPost(object));

    $('#view-post').append(closeContainer());
    $.each(object.array,function(index,value){
        $('#tag').append( renderTags(value));
    });
}
function openContainer()
{
    var render = '<div style="margin-left:80%; " class="w-50 mb-5 ">'
    return render;
}
function renderTags(value)
{
    var tag  = "/tag-"+$.trim(value.idtag)+"/"+$.trim(value.tags).toLowerCase()+"";
    var tagurlClean = cleanSpecialChar(tag);
    var render = '<a class="tags-link " href="'+tagurlClean+'"><span  class="badge badge-info mr-1">'+value.tags+'</span></a>';
    // $('#fullHeightModalTop').css('display','none');
    return render;
}
function closeContainer()
{
    var render =  '</div><hr class="hr-bold">'
    return render
}
function cleanSpecialChar(url)
{
    var specialChars = "!@#$^&%*()+=[]\{}|:<>?,";
    for (var i = 0; i < specialChars.length; i++) {
        url = url.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
    }
    url = url.toLowerCase();
    // Quitamos espacios y los sustituimos por _ porque nos gusta mas asi
    url = url.replace(/ /g,"_");
    // Quitamos acentos y "ñ". Fijate en que va sin comillas el primer parametro
    url = url.replace(/á/gi,"a");
    url = url.replace(/é/gi,"e");
    url = url.replace(/í/gi,"i");
    url = url.replace(/ó/gi,"o");
    url = url.replace(/ú/gi,"u");
    url = url.replace(/ñ/gi,"n");
    return url;
}
function renderPost(object)
{
    var rend = '';
    var url = "/post-"+$.trim(object.post.id)+"/"+$.trim(object.post.info)+".html";
    var cleanUrlSpecialChar = cleanSpecialChar(url);
    if ($(window).width() > 1200  )
    {
        rend = componentDesktop(object,cleanUrlSpecialChar);
        storageLocal();
    }else if($(window).width() > 600 && $(window).width() < 1200)
    {
        rend = componentsTablet(object,cleanUrlSpecialChar);
        storageLocal();
    }else if($(window).width() < 600)
    {
        rend = componentsMobile(object,cleanUrlSpecialChar);
        $('.top').css('width','100%');
        storageLocal();
    }
    return rend;
}
async function storageLocal()
{
    for(let i=0; i<window.localStorage.length;i++)
    {
        $('#'+window.localStorage.key(i)).attr("disabled", true);
        $('.'+window.localStorage.key(i)).attr("onclick", '');
        $('#'+window.localStorage.key(i)).css('opacity','0.6');
        $('.'+window.localStorage.key(i)).css('opacity','0.6');
        await getLike(window.localStorage.key(i)).then(o => {
            $('#'+window.localStorage.key(i)).find('span').text(o+" Votos");
            $('.'+window.localStorage.key(i)).find('span').text(o+" Votado");
            $('#'+window.localStorage.key(i)).find('span').addClass('badge-danger');
            $('#'+window.localStorage.key(i)).find('span').removeClass('badge-success');
            // $('#'+window.localStorage.key(i)).find('span').attr('class',' p-1');
        });
    }
}
function FigureCaptionController(object)
{
    var figurecaption = '';
    if(object.post.captionimage != null)
    {
        figurecaption = object.post.captionimage;

        setMetaDescription(figurecaption)
    }else
    {
        figurecaption = titleTags;
        setMetaDescription(figurecaption)
    }
    return figurecaption;
}
function altImageController(object)
{
    var altimage = '';
    if(object.post.altimage != null)
    {
        altimage = object.post.altimage;
    }else
    {
        altimage = 'Humor y amor para el mundo imagen de ';
    }
    return altimage;
}
function componentsTablet(object,cleanUrlSpecialChar)
{
    $('#view-post').removeClass('p-4');
    $('#view-post').removeClass('w-responsive');
    var f = new Date(object.post.date);
    dayOfTheWeek(f);
    var figurecaption = ''
    var altimage = '';
    if(object.post.likes == null)
    {
        object.post.likes = 'Votar';
    }
    figurecaption = FigureCaptionController(object);
    altimage = altImageController(object);
    var url = 'https://menudosgift.es'+encodeURIComponent(cleanUrlSpecialChar);
    var rend =  '<div class="d-flex flex-column justify-content-center align-items-center  mt-5 entry">'+
        '<div  class="media-body bodito">'+
        '<itemscope itemtype="http://schema.org/CreativeWork">'+
        '<a title="'+object.post.info+'" class="link-unique-post" href="'+cleanUrlSpecialChar+'"><h2  itemprop="title" style="text-align: center; font-size: 18px" class="mt-0 mb-1 font-weight-bold ">'+object.post.info+'</h2></a>'+
        '<span itemprop="datePublished" class="p-lead">'+dayOfTheWeek(f)+' - '+f.getDate()+'/'+(f.getMonth()+1)+'/'+f.getFullYear()+'</span>'+
        //'<p class="p-lead">'+object.post.idcategory+'</p>'+
        '</div>'+
        '<figure  class=" d-flex flex-column align-items-center justify-content-center">'+
        '<img itemprop="image" title="'+object.post.info+'" alt="'+altimage+' | '+object.post.info+'" loading="" class="mr-3 w-responsive" src="data:image/png;base64,'+object.post.image+'" >'+
        '<figcaption style="font-size: 14px; text-align:justify" itemprop="description" class="p-3"  >'+object.post.imagedescription+' </figcaption>'+
        '</figure>'+
        '<p style="font-size: 18px; text-align:justify" itemprop="description" class="p-3"  >'+figurecaption+' | '+object.post.info+'</p>'+
        '<div class="whatlinks">'+
        '<ul style="list-style: none;" class="d-flex flex-row align-items-center justify-content-center flex-wrap list-group mb-1">'+
        //'<li><a class="bg-dark whatsappLink desktop list-group-item" href="https://api.whatsapp.com/send?text='+object.post.info+' Bienvenido a Menudos Gift https://menudosgift.es"><i  style="font-size: 25px"  class="fab fa-whatsapp  text-success" ></i></a></li>'+
        '<ul  style="list-style: none; " class="list-group">' +
        '<li><button  type="button" class="btn btn-sm"  onclick="upLike(this)"  id="'+object.post.id+'" data-value=1><span style="font-size: 12px;z-index:-10" class="text-white bg-success p-1 ">'+object.post.likes+' Me mola</span></button></li>'+
        '</ul>'+
        '<li><a  class="btn btn-sm btn-success" class="whatsappLink mobile"   href="whatsapp://send?text=Menudos Gift comparte tu imagen favorita '+encodeURIComponent(object.post.info)+'%0D%0A%0D%0Ahttps%3A%2F%2Fmenudosgift.es'+encodeURIComponent(cleanUrlSpecialChar)+'" data-action="share/whatsapp/share"><span>Whatsapp</span></a></li>'+
        '<li><button type="button" class="btn btn-sm btn-primary" onclick="metas(this)" id="'+cleanUrlSpecialChar+'" data-value="'+object.post.info+'" target=’_blanK’> <span>Facebook</span></button></li>'+
        '<li><a href="http://twitter.com/intent/tweet?url='+url+'" data-param-description="'+object.post.info+'" data-param-href="'+url+'" data-param-url="'+url+'" data-param-text="'+object.post.info+'" class="btn btn-sm btn-info"  id="'+cleanUrlSpecialChar+'" data-value="'+object.post.info+'" target=’_blanK’><span>Twitter</span></button></li>'+
        '<li><a class="btn btn-sm bg-danger" href="http://pinterest.com/pin/create/button?url=https://menudosgift.es'+cleanUrlSpecialChar+'/&media=https://menudosgift.es/admin/images/'+urlImage(cleanUrlSpecialChar)+'.jpg" class="btn btn-sm btn-info"  data-value="'+object.post.info+'" target=’_blanK’><span class="text-white">Pinterest</span></button></li>'+
        '</ul>'+
        '</div>'+
        '</div>'
    if ('loading' in HTMLImageElement.prototype) {
        $('img').attr('loading','lazy');
    }
    return rend;
}
function componentsMobile(object,cleanUrlSpecialChar)
{
    var f = new Date(object.post.date);
    dayOfTheWeek(f);
    $('.top').css('width','100%');
    $('#view-post').addClass('ml-2');
    var figurecaption = '';
    var altimage = '';
    var figurecaption = ''
    if(object.post.likes == null)
    {
        object.post.likes = 'Votar';
    }
    figurecaption = FigureCaptionController(object);
    altimage = altImageController(object);
    var url = 'https://menudosgift.es'+encodeURIComponent(cleanUrlSpecialChar);
    var rend =  '<div itemscope class="d-flex flex-column justify-content-center align-items-center  mt-5 entry">'+
        '<div class="media-body bodito">'+
        '<itemscope itemtype="http://schema.org/CreativeWork">'+
        '<a  title="'+object.post.info+'" class="link-unique-post" href="'+cleanUrlSpecialChar+'"><h2  itemprop="title" style="text-align: center;font-size: 18px" class="mt-0 mb-1 font-weight-bold ">'+object.post.info+'</h2></a>'+
        '<span itemprop="datePublished" class="p-lead">'+dayOfTheWeek(f)+' - '+f.getDate()+'/'+(f.getMonth()+1)+'/'+f.getFullYear()+'</span>'+
        //'<p class="p-lead">'+object.post.idcategory+'</p>'+
        '</div>'+
        '<figure>'+
        '<img itemprop="image" title="'+object.post.info+'" alt="'+altimage+' | '+object.post.info+'" loading="" class="mr-3 w-responsive" src="data:image/png;base64,'+object.post.image+'" >'+
        '<figcaption style="font-size: 14px; text-align:center" itemprop="description" class="p-3"  >'+object.post.imagedescription+' </figcaption>'+
        '</figure>'+
        '<p style="font-size: 18px;text-align:justify" itemprop="description" class="p-3">'+figurecaption+' | '+object.post.info+'</p>'+
        '<ul  style="list-style: none; " class="list-group">' +
        '<li><button  type="button" class="btn btn-sm"  onclick="upLike(this)"  id="'+object.post.id+'" data-value=1><span style="font-size: 16px;z-index:-10" class="text-white bg-success p-1 ">'+object.post.likes+' Me mola</span></button></li>'+
        '</ul>'+
        '<ul style="box-sizing:border-box; list-style: none;" class="d-flex flex-row align-items-center justify-content-center flex-wrap list-group mb-1">'+
        '<li><a style="width:108px" class="btn btn-sm btn-success" class="whatsappLink mobile"   href="whatsapp://send?text=Menudos Gift comparte tu imagen favorita '+encodeURIComponent(object.post.info)+'%0D%0A%0D%0Ahttps%3A%2F%2Fmenudosgift.es'+encodeURIComponent(cleanUrlSpecialChar)+'" data-action="share/whatsapp/share"><span>Whatsapp</span></a></li>'+
        '<li><button  style="width:108px" type="button" class="btn btn-sm btn-primary" onclick="metas(this)" id="'+cleanUrlSpecialChar+'" data-value="'+object.post.info+'" target=’_blanK’> <span>Facebook</span></button></li>'+
        '<li><a style="width:108px"  href="http://twitter.com/intent/tweet?url='+url+'" data-param-description="'+object.post.info+'" data-param-href="'+url+'" data-param-url="'+url+'" data-param-text="'+object.post.info+'" class="btn btn-sm btn-info"  id="'+cleanUrlSpecialChar+'" data-value="'+object.post.info+'" target=’_blanK’><span>Twitter</span></button></li>'+
        '<li><a  style="width:109px" class="btn btn-sm bg-danger" href="http://pinterest.com/pin/create/button?url=https://menudosgift.es'+cleanUrlSpecialChar+'/&media=https://menudosgift.es/admin/images/'+urlImage(cleanUrlSpecialChar)+'.jpg" class="btn btn-sm btn-info"  data-value="'+object.post.info+'" target=’_blanK’><span class="text-white">Pinterest</span></button></li>'+
        '</ul>'+
        '</div>'+
        '</div>'
    if ('loading' in HTMLImageElement.prototype) {
        $('img').attr('loading','lazy');
    }
    return rend;
}
function componentDesktop(object,cleanUrlSpecialChar)
{
    var f = new Date(object.post.date);
    dayOfTheWeek(f);
    var figurecaption = '';
    var altimage = '';

    if(object.post.likes == null)
    {
        object.post.likes = 'Votar';
    }
    figurecaption = FigureCaptionController(object);
    altimage = altImageController(object);
    var url = 'https://menudosgift.es'+encodeURIComponent(cleanUrlSpecialChar);
    var rend =  '<div  class="d-flex flex-column justify-content-center align-items-center  mt-5 entry">'+
        '<div class="media-body bodito">'+
        '<itemscope itemtype="http://schema.org/CreativeWork">'+
        '<a title="'+object.post.info+'" class="link-unique-post" href="'+cleanUrlSpecialChar+'"><h2  itemprop="title" style="text-align: center; font-size: 18px" class="mt-0 mb-1 font-weight-bold ">'+object.post.info+'</h2></a>'+
        '<span itemprop="datePublished" class="p-lead">'+dayOfTheWeek(f)+' - '+f.getDate()+'/'+(f.getMonth()+1)+'/'+f.getFullYear()+'</span>'+
        //'<p class="p-lead">'+object.post.idcategory+'</p>'+
        '</div>'+
        '<figure  class="w-100 d-flex flex-column align-items-center justify-content-center">'+
        '<img itemprop="image" title="'+object.post.info+'" alt="'+altimage+' | '+object.post.info+'" loading="" class="mr-3 w-responsive" src="data:image/png;base64,'+object.post.image+'" >'+
        '<figcaption style="font-size: 14px; text-align:justify" itemprop="description" class="p-3"  >'+object.post.imagedescription+' </figcaption>'+
        '</figure>'+
        '<p style="font-size: 18px; text-align:justify" itemprop="description" class="p-3" >'+figurecaption+' | '+object.post.info+'</p>'+
        '<div class="whatlinks">'+
        '<ul style="list-style: none;" class="d-flex flex-row align-items-center list-group mb-4">'+
        //'<li><a class="bg-dark whatsappLink desktop list-group-item" href="https://api.whatsapp.com/send?text='+object.post.info+' Bienvenido a Menudos Gift https://menudosgift.es"><i  style="font-size: 25px"  class="fab fa-whatsapp  text-success" ></i></a></li>'+
        '<li><button  type="button" class="btn btn-sm btn-link"  onclick="upLike(this)"  id="'+object.post.id+'" data-value=1><span style="font-size: 16px" class="badge badge-success ">'+object.post.likes+' Me mola</span></button></li>'+
        '<li><button type="button" class="btn btn-sm btn-primary" onclick="metas(this)" id="'+cleanUrlSpecialChar+'" data-value="'+object.post.info+'" target=’_blanK’> <span>Facebook</span></button></li>'+
        '<li><a href="http://twitter.com/intent/tweet?url='+url+'" data-param-description="'+object.post.info+'" data-param-href="'+url+'" data-param-url="'+url+'" data-param-text="'+object.post.info+'" class="btn btn-sm btn-info"  id="'+cleanUrlSpecialChar+'" data-value="'+object.post.info+'" target=’_blanK’><span>Twitter</span></button></li>'+
        '<li><a class="btn btn-sm bg-danger" href="http://pinterest.com/pin/create/button?url=https://menudosgift.es'+cleanUrlSpecialChar+'/&media=https://menudosgift.es/admin/images/'+urlImage(cleanUrlSpecialChar)+'.jpg" class="btn btn-sm btn-info"  data-value="'+object.post.info+'" target=’_blanK’><span class="text-white">Pinterest</span></button></li>'+
        '</ul>'+
        '</div>'+
        '</div>'
    if ('loading' in HTMLImageElement.prototype) {
        $('img').attr('loading','lazy');
    }
    return rend;
}
function urlImage(urlimage)
{
    var split = urlimage.split("/");
    return split[2].split(".")[0]
}
function updateLike(id)
{
    return new Promise((resolve,reject) => {
        $.post('../bd/updateLikes.php',{"uplike":id},function(response){
            resolve(response);
        });
    });
}
function upLike(clickLike)
{
    if($(clickLike).attr('id'))
    {
        window.localStorage.setItem($(clickLike).attr('id'),$(clickLike).attr('id'));
        updateLike($(clickLike).attr('id')).then(o => {
            $(clickLike).find('span').text(o);
            storageLocal();
        });
    }else
    {
        window.localStorage.setItem($(clickLike).attr('class'),$(clickLike).attr('class'));
        updateLike($(clickLike).attr('class')).then(o => {
            $(clickLike).find('span').text(o+" Votado");
            storageLocal();
        });
    }
}
function metas(row)
{
    $(location).attr('href','https://www.facebook.com/sharer.php?u=https://menudosgift.es'+$(row).attr('id')+'&summary=sumario&nbspsobre&nbspmenudos&nbspgift&p[title]='+encodeURIComponent($(row).data('value'))+'&description=estaesmidescripcion&picture=https://cdn.pixabay.com/photo/2018/11/11/19/46/christmas-bauble-3809544_960_720.jpg');
}
function getLike(id) {
    return new Promise((resolve, reject) => {
        $.post("../bd/updateLikes.php", {"querylike": id}, function (response) {
            resolve(response)
        });
    })
}
function dayOfTheWeek(fdate) {
    var day = "";
    if (fdate.getDay() == 1) {
        day = "Lunes";
    } else if (fdate.getDay() == 2) {
        day = "Martes";
    } else if (fdate.getDay() == 3) {
        day = "Miercoles";
    } else if (fdate.getDay() == 4) {
        day = "Jueves";

    } else if (fdate.getDay() == 5) {
        day = "Viernes";
    } else if (fdate.getDay() == 6) {
        day = "Sábado";
    } else if (fdate.getDay() == 0) {
        day = "Domingo";
    }
    return day;
}

function cleanUrlId(url) {
    var urlsplit = url.split("/");
    var id = urlsplit[3].split('-');
    return id[1];
}

function cleanUrlCategory(url) {
    var urlsplit = url.split("/");
    var category = urlsplit[4];
    var split = '';
    if (category.indexOf('_') != -1) {
        split = category.replace(/_/gi, ' ')
    } else {
        split = category;
    }
    return title + " " + split.replace(/.html/, '');
}

function queryPostByTag(id) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'GET',
            url: '/bd/post-by-tag.php',
            data: {'id': id},
            beforeSend: function () {
                $('#carga').show();
            },
            success: function (response) {
                resolve(response);
            },
            error: function (xhr) { // if error occured
                alert("Error occured.please try again");
            },
            complete: function () {
                $('#loading').removeClass('spinner-border');
                $('#view-post').removeClass('class', 'bg-white');
                $('#view-post').attr('class', 'bg-light');
            }
        })
    })
}
function deleteRow(elem) {
    var opcion = confirm("Está seguro de borrar el registro");
    if (opcion == true) {
        var idrow = $(elem).parent().find('input:hidden').val();
        $('.panel').empty();
        $.get('../bd/delete-post.php', {'deleterow': idrow}, function (response) {
            if (response == "ok") {
                panelPostQuery();
            }
        });
    }
}

function linkDelete() {
    $('#delete').append('<a onclick="panelQuery(this)"  id="delete-post" href="#"><span class="mr-4">Borrar Posts</span></a>')
    $('#delete').append('<a onclick="panelQuery(this)"  id = "delete-tag" href="#"><span>Borrar Tags</span></a><br><br>')
}

function panelQuery(element) {
    $('#meesagempty').remove();
    if ($(element).attr('id') == 'delete-post') {
        $('.panel').remove();
        $(element).addClass('disabled');
        $(element).css('color', 'red');
        $('#delete-tag').css('color', 'black');
        $('#delete-tag').removeClass('disabled');
        panelPostQuery()
    } else if ($(element).attr('id') == 'delete-tag') {
        $('.panel').remove();
        $(element).addClass('disabled');
        $('#delete-post').removeClass('disabled');
        $(element).css('color', 'red');
        $('#delete-post').css('color', 'black');
        viewTagsPanel().then(o => {
            $.each(o, function (index, value) {
                $('#delete').append('<div  class="panel form-group panel mb-5 border p-1 " id="hideid' + value.id + '" class="w-100 mr-4 custom-control custom-radio">' +
                    '<label class="text-center">' + value.tag + '</label>' +
                    '<input type="hidden"  value="' + value.id + '">' +
                    ' <button onclick="deleteTag(this)" class="btn btn-teal   btn-sm btn-elegant" type="button" id="send-button-tag">Borrar</button>' +
                    '</div> ');
            });
        });

    }
}
function deleteTag(element) {
    const conf = confirm("está seguro de borrar el registro?");
    if (conf == true) {
        var idtag = $(element).parent().find('input:hidden').val();
        $('.panel').remove();
        $.get('../bd/delete-post.php', {"deletetag": idtag}, function (response) {

        });
        responseJsonTags.forEach(function (tag, index, object) {
            if (tag.id == idtag) {
                object.splice(index, 1);
            }
        });
        recoveryTags();
        panelQuery('#delete-tag')
    } else {
        alert("no ha sido posible borrar ");
    }
}
function panelPostQuery() {
    $('#delete').empty();
    linkDelete();
    $('#delete-post').css('color', 'red');
    $('#delete-tag').css('color', 'black');
    $.post('../bd/query-post.php', {"allpost": "all"}, function (response) {
        if (JSON.parse(response) == '') {
            $('#delete').append('<span id="meesagempty">No hay ningún post guardado</span>')
        } else {
            var resp = JSON.parse(response)
            $.each(resp, function (index, value) {
                $('#delete').css('display', 'grid');
                $('#delete').css('grid-template-columns', '1fr 1fr 1fr');
                $('#delete').css('gap', '1em');
                $('#delete label').css('justify-content:center');
                $('#delete').css('text-align:center');
                $('#delete').append('<div class="panel mb-5 border p-1" id="hideid' + value.id + '" class="w-100 custom-control custom-radio">' +
                    '<label>' + value.info + '</label>' +
                    '<input type="hidden"  value="' + value.id + '">' +
                    ' <button onclick="deleteRow(this)" class="btn btn-teal btn-mdb-color btn-block btn-sm btn-elegant" type="button" id="send-button">Borrar</button>' +
                    '</div> ');
            });
        }
    })
}
function searchPostByCategory(idcategory) {

    $('#view-post').empty();
    $.get('../bd/query-categories.php', {"postsbycategory": idcategory}, function (response) {
        $.each(JSON.parse(response), function (index, value) {
            // queryPostById(value.id);
            var objectPost = {"post":value};
            renderController(objectPost);
            //dynamicAlt(objectPost,idcategory);
        });
    });
}
async function dynamicAlt(objectPost,idcategory)
{
    if(idcategory == 4 && objectPost.post.altimage == null)
    {
        await  $('#view-post').find('img').attr('alt','Frases famosas, y humor para reir y reflexionar |'+objectPost.post.captionimage);
    }else if(idcategory == 4 && objectPost.post.captionimage == null )
    {
        await  $('#view-post').find('figcaption').text("Frases famosas y del día a día, y con sentido del humor |"+objectPost.post.altimage+" del sitio web https://www.menudosgift.es");
    }
}
function setTitle(str) {
    $('title').text(str);
}
function setMetaDescription(str) {
    $("meta[name='description']").attr("content", str);
}
function getCategories() {
    queryCategories().then(o => {
        renderCategories(JSON.parse(o));
    });
}
function getSideBar() {
    latestPosts().then(r => {
        $.each(r, function (index, value) {
            $('.sidebar').append(renderLatests(value));
            // $('#seccion').append(renderLatestResponsive(value));
        });
    });
}
function latestPosts() {
    return new Promise((resolve, reject) => {
        $.post('../bd/query-post.php', {'latests': 'latest'}, function (response) {
            resolve(JSON.parse(response));
        });
    });
}
function zIndex()
{
    var index = 0;
    if(!onOffSeeTag())
    {
        index = -10;
    }else
    {
        index = 0;
    }
    return index;
}
function voteString()
{
    var vote = '';
    if(zIndex() < 0)
    {
        vote = "Votos";
    }else
    {
        vote = "Votar";
    }
    return vote;
}
function renderLatests(resp) {
    var url = "/post-" + $.trim(resp.id) + "/" + $.trim(resp.info) + ".html";
    var cleanUrlSpecialChar = cleanSpecialChar(url);
    if (resp.likes == null) {
        resp.likes = 'Votar';
    }
    var latests = '<!-- Card -->\n' +
        '<div class="card ml-1 bg-light toplatest ">\n' +
        '\n' +
        '  <!-- Card content -->\n' +
        '  <div style="gap:1em;"  class="card-body d-flex flex-lg-column flex-md-column flex-xl-row align-items-md-left justify-content-left ">\n' +
        '\n' +
        '    <!-- Avatar -->\n' +
        '    <img  loading="lazy" src="data:image/png;base64,' + resp.image + '" class="card-img-100" height="auto"  alt="avatar">\n' +
        '\n' +
        '    <!-- Content -->\n' +
        '    <div >\n' +
        '\n' +
        '      <!-- Title -->\n' +
        '      <a href="' + cleanUrlSpecialChar + '"><h6 style="font-size: 12px" class="card-title text-dark font-weight-bold mb-2">' + resp.info + '</h6></a>\n' +
        '      <!-- Subtitle -->\n' +
        '      <p class="card-text"><i class="far fa-clock pr-2"></i>' + resp.date+ '</p>\n' +
        '\n' +
        '    </div>\n' +
        '\n' +
        '  </div>\n' +
        '\n' +
        '  <!-- Card content -->\n' +
        '  <div class="card-body ml-5">\n' +
        '\n' +
        '\n' +
        '    <div class="collapse-content">\n' +
        '\n' +
        '    </div>\n' +
        '\n' +
        '  </div>\n' +
        '\n' +
        '  <a    type="button" onclick="upLike(this)"  class="' + resp.id + ' "  data-value=1><i class="fas fa-thumbs-up text-danger float-right p-1 " data-toggle="tooltip" data-placement="top" title="I like it"><span class="badge-pill badge-success ml-1 ">' + resp.likes + " "+voteString()+' </span></i></a>\n' +
        '</div>\n' +
        '<!-- Card '
    return latests;
}
//card-body d-flex flex-lg-column flex-md-column flex-xl-row align-items-md-left justify-content-left
function getTopBar() {
    $.post('../bd/query-post.php', {'top': 'top'}, function (response) {
        sendTopSession(JSON.parse(response)).then(o => alert(o))
        $.each(JSON.parse(response), function (index, value) {
            $('.top').append(renderLatests(value));
        });
    });
}
function sendTopSession(topest)
{
    return new Promise((resolve,reject)=>{
        $.post('../php/topsession.php',{"topsession":topest},function(response){
            resolve(JSON.parse(response));
        });
    });
}
function getTopSession()
{
    return new Promise((resolve,reject)=>{
        $.post('../php/topsession.php',{"getTop":1},function(response){
            resolve(JSON.parse(response))
        });
    })
}

function viewTagsPanel() {
    return new Promise((resolve, reject) => {
        $.get('../bd/query-tags.php', {"querytags": 1}, function (response) {
            resolve(JSON.parse(response));
        });
    });
}
function getLinksModal()
{
    $('#container-links-modal').append('<ul id="profile-link-nav" style="list-style: none" class="d-flex flex-row align-items-center justify-content-around w-100 mt-3 lead navbar-nav mr-auto row">'+
        '<li><a class="default-link" id="profile-link-image" data-value="formulary">Sube tu imagen de perfil</a></li>'+
        '<li><a id="profile-link-quote" data-value="text-area">Descripción</a></li>'+
        '<li><a id="profile-link-post"  data-value="post">Escribe tu comentario</a></li>'+
        '<li><a id="profile-link-edit"  data-value="delete">Borrar  registros</a></li>'+
        '<li><a id="profile-link-edit-post"  data-value="delete">Editar  registros</a></li>'+
        '</ul>');
}
function cookiesInfo()
{
    $('.accept').on('click',function(){
        localStorage.setItem("modalCerrado",true);
        $('#cookieModal').modal("hide");
    });
    if(localStorage.getItem('modalCerrado'))
    {
        $('#cookieModal').modal("hide");
    }else if (localStorage.getItem('modalCerrado') === null)
    {
        $('#cookieModal').modal("toggle");
    }
}
var contbool = 0;
function moveTag()
{
    contbool++;
    $('.seccion').toggle();
    if(onOffSeeTag() )
    {
        $('.seetag').text('Buscar temas');
        $('.seetag').css('font-size','18px');
        $('.top').css('position','relative');
        $('.top').css('z-index','0');
    }else
    {
        $('.seetag').text('Cerrar temas');
        $('.seetag').css('font-size','18px');
        $('.top').css('z-index','-10');
    }
}
function onOffSeeTag()
{
    var bok = false;
    if(contbool%2 == 0)
    {
        bok = true;
    }
    return bok;
}
function editPosts()
{
    return new Promise ((resolve,reject) => {
        $.post('../bd/query-post.php',{"allpost":1},function(response){
            resolve(JSON.parse(response))
        })
    });
}
function viewEditTags(element)
{
    queryPostTag($(element).data('value')).then(o =>   {

    });
}
var contPageSection = 0;
function nextTagSection()
{
    $('.seccion').empty();
    if($(window).width() < 900)
    {
        $('.seccion').append( '<button style=" position:absolute;top:5px;left:180px" type="button" class="close btn-block " data-dismiss="modal" aria-label="Close">'+
            '&times;'+
            '</button>'
        );
    }
    viewTagsPanel().then(o => {
        $.each(o, function (index, value) {
            if (index > tagsPaginate-1) {
                var url = '/tag-' + value.id + '/' + value.tag;
                var cleanUrlSpecialChar = cleanSpecialChar(url);
                $('.seccion').append('<a href="' + cleanUrlSpecialChar + '" data-value="' + value.id + '"  href="#">' +
                    '<span style="width:120px;" class="badge badge-default text-dark p-2 ">'
                    + value.tag.substr(0,18) +
                    '</span>' +
                    '</a>');
            }
        });
        $('.seccion').append('<a onclick="preTagSection()" class="nav-link text-monospace" href="#"><span  style="font-size:20px" class="badge-success badge badge-pill"><<</span></a>')
    });
}
function closeSection()
{
    moveTag();
}
function preTagSection()
{
    $('.seccion').empty();
    if($(window).width() < 900)
    {
        $('.seccion').append( '<button style=" position:absolute;top:5px;left:180px" type="button" class="close btn-block " data-dismiss="modal" aria-label="Close">'+
            '&times;'+
            '</button>'
        );
    }
    viewTagsPanel().then(o => {
        $.each(o, function (index, value) {

            if (index < tagsPaginate) {
                var url = '/tag-' + value.id + '/' + value.tag;
                var cleanUrlSpecialChar = cleanSpecialChar(url);
                $('.seccion').append('<a href="' + cleanUrlSpecialChar + '" data-value="' + value.id + '"  href="#">' +
                    '<span style="width:120px;" class="badge badge-default text-dark p-2 ">'
                    + value.tag +
                    '</span>' +
                    '</a>');
            }
        });
        $('.seccion').append('<a onclick="nextTagSection()" class="nav-link text-monospace" href="#"><span style="font-size:20px" class="badge-success badge badge-pill">>></span></a>')
    });
}
$(document).ready(function () {
    if ('loading' in HTMLImageElement.prototype) {

    }

    $('head').append( '<meta name="description" content="caca de mono puede perder">' );
    contbool = 0;
    $('#carga').show();
    $('#Google').hide();
    $('.seccion').hide();
    cookiesInfo();
    $('.entry').empty();
    checkSession();
    setMetaDescription(description);
    getLinksModal();
    viewTagsPanel().then(o => {
        countTags = o.length;
        $.each(o, function (index, value) {
            var url = '/tag-' + value.id + '/' + value.tag;
            var cleanUrlSpecialChar = cleanSpecialChar(url);
            if(index < tagsPaginate)
            {
                $('.seccion').append('<a href="' + cleanUrlSpecialChar + '" data-value="' + value.id + '"  href="#">' +
                    '<span style="width:120px;" class="badge badge-default text-dark p-2 ">'
                    + value.tag +
                    '</span>'+
                    '</a>');
            }

        });
        if(countTags >tagsPaginate)
        {
            $('.seccion').append('<a onclick="nextTagSection()" class="nav-link text-monospace" href="#"><span style="font-size: 20px" class="badge-success badge badge-pill">>></span></a>')
        }
        if($(window).width() < 1000)
        {
            getTopBar();
            //$('#bests-title').remove();
            $('.latests').remove();
            $('.seccion button').show();

        }else
        {
            $('.seetag').remove();
            $('.seccion').show();
            $('.seccion').css('position','relative');
            $('.seccion').removeClass('deep-blue-gradient');
            $('.seccion').addClass('card');
            getSideBar();
            sessionTopController();
            $('.seccion button').hide();
        }

    });

    function sessionTopController()
    {
        getTopSession().then(o =>{
            if(o)
            {
                $.each(o,function(index,value){
                    $('.top').append(renderLatests(value));
                });
            }else
            {
                getTopBar();
            }
        });
    }
    queryCategories().then(categories => {
        $.each(JSON.parse(categories), function (index, value) {
            var url = '/category-' + value.id + '/' + value.category;
            var cleanUrlSpecialChar = cleanSpecialChar(url);
            $('#list-menu').append('<li style="text-transform: capitalize;" class="text-light nav-item"><a  href="' + cleanUrlSpecialChar + '" data-value="' + value.id + '" class="nav-link" href="#">' + value.category + '</a></li>');
        });
    });
    var full_url = document.URL;
    if (full_url.indexOf('-') == -1)
    {
        queryPosts();

    } else if (full_url.indexOf('tag') != -1)
    {
        var id = cleanUrlId(full_url);
        var category = cleanUrlCategory(full_url);
        setTitle(" "+category + "  ")
        setMetaDescription(descriptionTags + " " + category);
        queryPostByTag(id).then(idpost => {
            var idpostSp = idpost.split(" ");
            $.each(idpostSp, function (index, value) {
                if (value != '') {
                    queryPostById(value,false);
                }
            });
            $('#carga').hide();
        });
    } else if (full_url.indexOf('post') != -1) {
        $('#carga').show();
        var id = cleanUrlId(full_url);
        var category = cleanUrlCategory(full_url);
        setTitle(" "+category);
        setMetaDescription(description + " " + category)
        queryPostById(id,true);
        $('#carga').hide();
    } else if (full_url.indexOf('category') != -1) {
        var id = cleanUrlId(full_url);
        var category = cleanUrlCategory(full_url);
        setTitle(" "+category);
        setMetaDescription(description + " " + category);
        searchPostByCategory(id);
        $('#carga').hide();
    }
    $('#profile-link-image').addClass('active');
    $('div .seetag').on('click',function(){
        moveTag();
    });
    $('a').on('click', function () {
        var social = $(this).attr('id');
        modalLinks = $(this).attr('id');
        getLinksId(modalLinks);
        //al clickar boton google
        if (social == 'Google') {
            $(location).attr('href', 'php/login.php');
        }
        //cierre de session
        if ($(this).attr('id') == 'close-session') {
            $.get('php/close-session.php', {'session': 'close'}, function (res) {
                if (res) {
                    $('#dropdown-menu').css('display', 'none');
                    location.reload();
                }
            });
        }
    });
    /*$('#link-search').on('click',function(){
        if($('#searching').val() != '')
        {
            var response = searchByTag();
            if(response.idtag == '')
            {
                alert("no encontramos la búsqueda solicitada ");
            }else
            {
                var url = '/tag-'+response.idtag+'/'+response.tags;
                location.href = cleanSpecialChar(url);
            }
        }else
        {
            alert("Búsqueda vacía");
        }
    });*/
    $('#submit-createtag-button').on('click', function () {
        disabledButtonTag();
        submitTags();
        $('#panelcategory').html(categories);
    });
    $('#submit-post-button').on('click', function () {
        if ($('#posts').val()) {
            recoveryUserSession().then(object =>
                submitInsertPost(object.iduser),
            );
        } else {
            alert("debe introducir una entrada");
        }
        // $('#submit-post-button').attr('disabled','true');
    });
    $('#posts').on('focus', function () {
        deleteLastTags();
        enabledButtonTag();
        $('#submit-post-button').removeAttr('disabled');
        responseJsonTags = [];
        recoveryTags();
        getCategories();
    });
    $('#submit-viewquery-button').on('click', function () {
        queryPosts();
    });
    $('#send-image-post').on('click', function () {
        imagePostUpload();
    })
    $('#profile-link-edit').on('click', function () {
        $('#delete').empty();
        panelPostQuery();
        // linkDelete();
        $('#delete-post').css('color', 'red');
        $('#delete-tag').css('color', 'black');
        // panelQuery();
    });
    $('#profile-link-edit-post').on('click',function(){
        editPosts().then(o => {
            $.each(o, function(index,value){
                $('#edit').append(
                    '<a onclick="viewEditTags(this)" data-value="'+value.id+'" class="form-check-label " for="materialGroupExample1">- '+value.info+' -</a>'
                );
            });
        });
    });
    $('#searching').on('click', function () {
        cont++;
        if (cont == 10) {
            $('#Google').show();
        }
    });
    $('.close').on('click',function(){
        moveTag()
    });

});
