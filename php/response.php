<?php
 error_reporting(E_ALL);
 ini_set("display_errors", 1);
    //https://console.developers.google.com/apis/dashboard?project=clear-idea-293513
    include '../vendor/autoload.php';
    include 'config.php';
    //include 'user.php';
    session_start();
    include '../bd/insert-user.php';
    include '../bd/check-avatar.php';
    $signdata = array();
    $client = new Google_Client();
    $client->setClientId($clientID);
    $client->setClientSecret($clientSecret);
    $client->setRedirectUri($redirectUri);
    $client->addScope("email");
    $client->addScope("profile");
    $image = '';
    if (isset($_GET['code'])) {
        $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
        $client->setAccessToken($token['access_token']);
        $user = new User();       
        // get profile info
        $google_oauth = new Google_Service_Oauth2($client);
        $google_account_info = $google_oauth->userinfo->get();
        $email =  $google_account_info->email;
        $name =  $google_account_info->name;
        $id = $google_account_info->id; 
        $image = $google_account_info->picture;
        $_SESSION['username'] = $user;
        $_SESSION['username']->name = $name;
        $_SESSION['username']->email = $email;
        $_SESSION['username']->id = $id;
        $_SESSION['username']->image = $image;
        $_SESSION['username']->avatar = avatarBase64();
        $_SESSION['username']->quote = recoveryQuote();
        print "<script> window.location = '../index.php';</script>";
      }
    function avatarBase64()
    {
      $user = $_SESSION['username'];
      $imagenComoBase64 = '';
      if(checkAvatar($user)==0)
      {
        $imgpath = "../admin/images/avatar-masculino.jpg";
        $imageContentBinary = file_get_contents($imgpath);
        $imagenComoBase64 = base64_encode($imageContentBinary);
      }else
      {
        $imagenComoBase64 = recoveryInfoUser($user)[0][4];

      }
      return   $imagenComoBase64;
    }

    function recoveryQuote()
    {
        $user = $_SESSION['username'];
        $recoveryQuote = recoveryInfoUser($user);
        return $recoveryQuote[0][7];
    }
?>
