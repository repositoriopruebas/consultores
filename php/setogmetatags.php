<?php
$params = array();
if(isset($_GET['post']))
{
    if($params['type'] == "") $params['type'] = "article";
    if($params['locale'] == "") $params['locale'] = "en_ES";
    if($params['title'] == "") $params['title'] = getTitle($_SERVER["REQUEST_URI"]);
    if($params['image'] == "") $params['image'] = getImage($_SERVER["REQUEST_URI"]);
    if($params['description'] == "") $params['description'] = "Tus memes más originales en español, humor y  amor para todo el mundo ".getTitle($_SERVER["REQUEST_URI"])." ";
}else
{
   if($params['type'] == "") $params['type'] = "article";
   if($params['title'] == "") $params['title'] = " Memes de humor y de amor  para todo el mundo ";
   if($params['description'] == "") $params['description'] = "MenudosGift contiene para ti los memes más  originales  en español  para que puedas compartir con todo el mundo";
   if($params['image'] == "") $params['image'] = "";
}
function getTitle($uri)
{

    $nombres = explode("/", $uri);
    $pretitle = explode(".",$nombres[2]);
    $result = '';
    if($pretitle[0] != '')
    {
        $result = str_replace('_',' ',$pretitle[0]);
    }
    return $result;
}
function getImage($urlimage)
{
    $nombres = explode("/", $urlimage);
    $preimage = explode(".",$nombres[2]);
    return "https://".$_SERVER["HTTP_HOST"]."/admin/images/".searchDir($preimage[0]);
}
function searchDir($image)
{
    $path  = 'admin/images';
    $dir = opendir($path);
    $nameimage = '';
    while ($current = readdir($dir)) {
        if($image.".jpg" == $current)
        {
            $nameimage = $current;
        }else if($image.".gif" == $current)
        {
            $nameimage = $current;
        }
    }
    return $nameimage;
}
function getNameImage($image)
{
    $path  = 'admin/images';
    $files = array_diff(scandir($path), array('.', '..'));
    $res = '';
    foreach($files as $file)
    {
        if(strpos($file,$image))
        {
            $res = $file;
        }
    }
    return $res;
}
?>